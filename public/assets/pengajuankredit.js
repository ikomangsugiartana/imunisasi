$(document).ready(function(){
	
    $('#pkPilihNasabah').change(function(){
        var id=$("#pkPilihNasabah").val();
        $.get('../pkgetDataNasabah/'+id,function(data, status){
            $('#pkPekerjaan').val(data.pekerjaan);
        });
    });

    
    // -----------------Pelanggan--------------
    var tbpengajuankredit = $('#tbpengajuankredit').DataTable({
        responsive:true
    });

    $('#modaldetailpegajuan').on('shown.bs.modal', function (e) {
        $('.overlay').css('display','block');
        var id = $(e.relatedTarget).data('id');
        $('#loadformpengajuan').load('pengajuankredit/'+id+'/detail');
        setTimeout(function() {
                $('.overlay').css('display','none');
        });
        });

    $("#btnvalidasi").on('click',function(){
        var data=$("#formdetailvalidasi").submit();

    });

     $("#btntambah").on('click',function(){
       $("#notif").addClass("show");
        setTimeout(function(){
        $("#notif").removeClass("show");
        },3000);
    });

     $('#modaldetailspk').on('shown.bs.modal', function (e) {
        $('.overlay').css('display','block');
        var id = $(e.relatedTarget).data('id');
        $('#loaddetailspk').load('detailspk/'+id);
        setTimeout(function() {
                $('.overlay').css('display','none');
        });
        });
});