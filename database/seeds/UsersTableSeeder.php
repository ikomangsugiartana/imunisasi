<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=\App\User::where('email','jasmiari@gmail.com')->first();
    	if ($user) {
    		print_r('data sudah disetup');
    	}else{
    		 \App\User::create([
	        'name'  => 'Jasmiari',
	        'email' => 'jasmiari@gmail.com',
            'username' => 'jasmiari',
            'role' => 1,
	        'password'  => bcrypt('jasmiari')
			]);
    	}
       
    }
}
