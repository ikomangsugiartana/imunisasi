@extends('layouts.template')
@section('bread')
    <h1>
        Setting <small>SMS</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Setting</a></li>
        <li class="active">SMS</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-body">
                  <?php 
                    $setting=App\Setting::get();
                   ?>
                   @if($setting->isEmpty())
                    <form method="post" action="{{route('setting.store')}}">
                      @csrf
                    @else
                    <form method="post" action="{{route('setting.update',[1])}}">
                      @csrf
                      @method('PUT')
                      @endif

                      <div class="form-group row">
                        <label class="col-md-2">Jam Pengiriman SMS</label>
                        <div class="col-md-10">
                          <input type="time" name="time" class="form-control" required="" value="@if(!$setting->isEmpty()){{$setting[0]->time}}@endif" id="time">
                        </div>
                      </div>
                       <div class="form-group row">
                        <label class="col-md-2">Pesan</label>
                        <div class="col-md-10">
                          <textarea class="form-control" rows="5" name="pesan" required="" > @if(!$setting->isEmpty()){{$setting[0]->pesan}}@endif</textarea>
                        </div>
                      </div>
                      <div style="text-align: right;">
                        <button class="btn btn-primary">Simpan</button>
                        <!-- <a class="btn btn-warning ">Testing</a> -->

                      </div>
                    </form>

                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')

@endpush

