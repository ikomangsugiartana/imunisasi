<!DOCTYPE html>
<html>
	@include('layouts.head')
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('layouts.top')
		<aside class="main-sidebar">
			@include('layouts.left')
		</aside>
		<div class="content-wrapper">
			<section class="content-header">
		      @yield('bread')
   			</section>
   			<section class="content container-fluid">

		      @yield('content')

		    </section>
		</div>
		<footer class="main-footer">
    		<!-- To the right -->
		    <div class="pull-right hidden-xs">
		      SPK v1.
		    </div>
    		<!-- Default to the left -->
    		<strong>Copyright &copy; 2016 <a href="#">LPD KUTUH</a>.</strong> 
  		</footer>
  		@include('layouts.right')

	</div>
	@include('layouts.foot')
	@stack('scripts')
</body>
</html>