<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
    <title>Login</title>

  	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<link rel="stylesheet" type="text/css" href="{{url('assets/plugin/bootstrap/dist/css/bootstrap.min.css')}}">
  	<link rel="stylesheet" type="text/css" href="{{url('assets/plugin/font-awesome/css/font-awesome.min.css')}}">
  	<link rel="stylesheet" type="text/css" href="{{url('assets/dist/css/AdminLTE.min.css')}}">
  	<link rel="stylesheet" type="text/css" href="{{url('assets/dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{url('assets/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="{{url('assets/plugin/datepicker/datepicker.css')}}">
  <link rel="stylesheet" href="{{url('assets/plugin/timepicker/bootstrap-timepicker.min.css')}}">
      <link rel="stylesheet" href="{{url('assets/as/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/as/responsive.bootstrap.min.css')}}">
  
  <link rel="stylesheet" href="{{url('css/custom.css')}}">



	<script src="{{url('assets/plugin/jquery/dist/jquery.min.js')}}"></script>

	


</head>