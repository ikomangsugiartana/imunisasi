<!DOCTYPE html>
<html>
	@include('layouts.head')
<body class="layout-top-nav skin-blue-light">
	<div class="wrapper">
		@include('layouts.topRegister')
		<div class="content-wrapper">
			<div class="container">
				<section class="content-header">
					@yield('bread')
				</section>
				<section class="content">
					@yield('content')
				</section>
				
			</div>
		</div>
		
		
	</div>
	@include('layouts.foot')
	@stack('scripts')
</body>
</html>
