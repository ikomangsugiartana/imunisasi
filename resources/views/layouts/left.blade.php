<section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('assets/dist/img/user.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" nama_kelas="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        @if(Auth::user()->role==1)
        <li ><a href="{{route('asistendokter.index')}}"><i class="fa fa-user"></i> <span>Asisten Dokter</span></a></li>
        <li ><a href="{{route('dokter.index')}}"><i class="fa  fa-user-md"></i> <span>Dokter</span></a></li>
        <li ><a href="{{route('pasien.index')}}"><i class="fa  fa-wheelchair"></i> <span>Pasien</span></a></li>
        <li ><a href="{{route('jenisimunisasi.index')}}"><i class="fa fa-tags"></i> <span>Jenis Imunisasi</span></a></li>
        <li ><a href="{{route('imunisasi.index')}}"><i class="fa fa-medkit"></i> <span>Imunisasi</span></a></li>
        <li ><a href="{{route('setting.index')}}"><i class="fa fa-gears"></i> <span>Setting</span></a></li>

        @endif
        <?php   
          $user=App\Pasien::where('user_id',Auth::user()->id)->first();
         ?>
         @if(Auth::user()->role==2)
        <li ><a href="{{route('jadwalfilter')}}"><i class="fa fa-edit"></i> <span>Jadwal Imunisasi</span></a></li>
        @endif
        @if(Auth::user()->role==3)
        <li ><a href="{{route('jadwalpasien',[$user->user_id])}}"><i class="fa fa-edit"></i> <span>Jadwal Imunisasi</span></a></li>
        @endif





        @if(Auth::user()->level==1)
        <li ><a href="{{url('dashboardNasabah')}}"><i class="fa fa-home"></i> <span>Home</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-link"></i>
            <span>Pengaturan Profil</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="{{url('profil')}}"><i class="fa fa-circle-o"></i><span>Profil</span></a></li>
            <!-- <li class=""><a onclick="pesan()" href="#"><i class="fa fa-circle-o"></i><span>Ubah password</span></a></li> -->

          </ul>
        </li>
        <li ><a href="{{route('pengajuankredit.index')}}"><i class="fa fa-dollar"></i> <span>Pengajuan Kredit</span></a></li>

        @endif

        @if(Auth::user()->level==2)
         <li class=""><a href="{{route('nasabah.index')}}"><i class="fa fa-group"></i><span>Data Nasabah</span></a></li>
            <li class=""><a href="{{route('karyawan.index')}}"><i class="fa fa-user"></i><span>Data Karyawan</span></a></li>
            <li class=""><a href="{{route('kreteria.index')}}"><i class="fa fa-list"></i><span>Data Kreteria</span></a></li>
        <li ><a href="{{route('pengajuankredit.index')}}"><i class="fa fa-dollar"></i> <span>Pengajuan Kredit
        <li ><a href="{{url('prosesSPK')}}"><i class="fa fa-desktop"></i> <span>SPK</span></a></li>
            <li class=""><a href="{{route('observasi.index')}}"><i class="fa fa-search"></i><span>Data Observasi</span></a></li>
        <li ><a href="{{url('validasi')}}"><i class="fa fa-check"></i> <span>Validasi Pengajuan Kredit</span></a></li>
        <li ><a href="{{url('datapeminjam')}}"><i class="fa fa-0"></i> <span>Data Peminjam</span></a></li>


        </span></a></li>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i>
              <span>Laporan</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href="{{url('laporankreteria')}}" target="_blank"><i class="fa fa-circle-o"></i>Data Kreteria</a></li>
              <li class=""><a href="{{url('laporankaryawan')}}" target="_blank"><i class="fa fa-circle-o"></i>Data Karyawan</a></li>
              <li class=""><a href="{{url('laporannasabah')}}" target="_blank"><i class="fa fa-circle-o"></i>Data Nasabah</a></li>
              <li class=""><a href="{{url('getlaporanobservasi')}}"><i class="fa fa-circle-o"></i> Data Observasi</a></li>
              <li class=""><a href="{{url('laporanpengajuankredit')}}"><i class="fa fa-circle-o"></i> Data Pengajuan Kredit</a></li>
              <li class=""><a href="{{url('laporandatapeminjam')}}"><i class="fa fa-circle-o"></i> Data Peminjam</a></li>



            </ul>
          </li>
          @endif
           
      </ul>
      <!-- /.sidebar-menu -->
</section>
<script type="text/javascript">
  function pesan(){
    alert("maaf fitur ini masih dalam pengerjaan");
  }
</script>