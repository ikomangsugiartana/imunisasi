@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Imunisasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Imunisasi</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Edit Data Imunisasi</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('imunisasi.update',[$imunisasi->id_imunisasi])}}">
                  @csrf
                  @method('PUT')
                <div class="box-body">
                   <div class="form-group">
                     <label class="col-md-2">Nama Imunisasi</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_imunisasi" class="form-control" required="" placeholder="nama imunisasi" value="{{$imunisasi->nama_imunisasi}}">
                     </div>
                   </div>

                   <div class="form-group">
                     <label class="col-md-2">Jenis Imunisasi</label>
                     <div class="col-md-10">
                      <select name="jenis_imunisasi" required="" class="form-control">
                        <option value="">pilih jenis imunisasi</option>
                        @foreach($jenis_imunisasi as $value)
                          <option value="{{$value->id}}" {{($value->id==$imunisasi->jenis_imunisasi)?'selected':''}}>{{$value->jenis_imunisasi}}</option>
                        @endforeach
                      </select>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-md-2">Umur</label>
                     <div class="col-md-10">
                       <input type="text" name="umur" class="form-control" required="" placeholder="umur" value="{{$imunisasi->umur}}"> 
                     </div>
                   </div>

                 </div>

                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

