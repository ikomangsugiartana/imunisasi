@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Imunisasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Imunisasi</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <a class="btn btn-primary pull-right" href="{{route('imunisasi.create')}}" data-toggle="modal">Tambah</a>
                </div>
                <div class="box-body">
                   <table class="table table-bordered" id="tb_imunisasi">
                       <thead>
                           <tr>
                              <th>Id Imunisasi</th>
                              <th>Jenis Imunisasi</th>
                              <th>Nama Imunisasi</th>
                              <th>Umur</th>
                              <th>Aksi</th>
                           </tr>
                       </thead>
                     
                       <tbody>
                        @forelse($imunisasi as $value)
                          <tr>
                            <td>{{$value->id_imunisasi}}</td>
                            <td>{{$value->getJenisImunisasi->jenis_imunisasi}}</td>
                            <td>{{$value->nama_imunisasi}}</td>
                            <td>{{$value->umur}}</td>
                            <th><a href="{{route('imunisasi.edit',[$value->id_imunisasi])}}"><span class="btn btn-warning"><i class="fa fa-edit"></i></span></a></th>


                          </tr>
                        @empty
                        @endforelse
                       
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      var tb_asdok = $('#tb_imunisasi').DataTable({
        responsive:true
      });
  });
</script>
@endpush

