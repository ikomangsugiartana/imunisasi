@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Dokter</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Edit Data Dokter</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('dokter.update',[$dokter->id_dokter])}}">
                  @csrf
                  @method('PUT')
                <div class="box-body">
                   <div class="form-group">
                     <label class="col-md-2">Nama Dokter</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_dokter" class="form-control" required="" placeholder="nama dokter" value="{{$dokter->nama_dokter}}">
                     </div>
                   </div>
                    <div class="form-group">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                        <select class="form-control" required="" name="jenis_kelamin">
                          <option value="">Pilih Jenis Kelmain</option>
                          <option value="1" {{($dokter->jenis_kelamin_dokter==1)?'selected':''}}>Laki-Laki</option>
                          <option value="0" {{($dokter->jenis_kelamin_dokter==0)?'selected':''}}>Perempuan</option>
                        </select>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-md-2">Alamat Dokter</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control">{{$dokter->alamat_dokter}}</textarea>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-md-2">Telepon</label>
                     <div class="col-md-10">
                       <input type="text" name="telepon" class="form-control" required="" placeholder="telepon" value="{{$dokter->no_tlp_dokter}}">
                       
                     </div>
                   </div>

                    <div class="form-group">
                     <label class="col-md-2">KTP</label>
                     <div class="col-md-10">
                       <input type="text" name="ktp" class="form-control" required="" placeholder="KTP" value="{{$dokter->no_ktp_dokter}}">
                      
                     </div>
                   </div>
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

