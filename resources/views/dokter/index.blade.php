@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Dokter</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <a class="btn btn-primary pull-right" href="{{route('dokter.create')}}" data-toggle="modal">Tambah</a>
                </div>
                <div class="box-body">
                   <table class="table table-bordered" id="tb_dokter">
                       <thead>
                           <tr>
                              <th>Id Dokter</th>
                              <th>Nama Dokter</th>
                              <th>Jenis Kelamin</th>
                              <th>Alamat</th>
                              <th>No Telepon</th>
                              <th>KTP</th>

                              <th>Aksi</th>
                           </tr>
                       </thead>
                     
                       <tbody>
                        @forelse($dokter as $as)
                          <tr>
                            <td>{{$as->id_dokter}}</td>
                            <td>{{$as->nama_dokter}}</td>
                            <td>
                              @if($as->jenis_kelamin_dokter==1)
                                Laki-Laki
                              @else
                                Perempuan
                              @endif
                            </td>
                            <td>{{$as->alamat_dokter}}</td>
                            <td>{{$as->no_tlp_dokter}}</td>
                            <td>{{$as->no_ktp_dokter}}</td>
                            <th><a href="{{route('dokter.edit',[$as->id_dokter])}}"><span class="btn btn-warning"><i class="fa fa-edit"></i></span></a></th>


                          </tr>
                        @empty
                        @endforelse
                       
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      var tbKaryawan = $('#tb_dokter').DataTable({
        responsive:true
      });
  });
</script>
@endpush

