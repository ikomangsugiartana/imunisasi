<!DOCTYPE html>
<html>
  @include('layouts.head')
  <style type="">
    .login-box-body{
      border-radius: 5px;
    }
    .login-box-body input,.login-box-body button{
        height: 48px;
        border-radius: 5px;
    }
  </style>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>APOTEK ASIA FARMA</b></a>

  </div>
  <!-- /.login-logo -->

  <div class="login-box-body">

    <p class="login-box-msg">Sistem Informasi Imunisasi</p>
    <form action="{{ route('login') }}" method="POST">
      @csrf
        <div class="form-group has-feedback @error('username') has-error @enderror">
          <p>Username</p>
          <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="username">
            @error('username')
                <span class="help-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group  has-feedback @error('password') has-error @enderror">
          <p>Password</p>
          <input id="password" type="password" class="form-control " name="password" required autocomplete="current-password" placeholder="password">
                @error('password')
                    <span class="help-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
  @include('layouts.foot')
</body>
</html>
