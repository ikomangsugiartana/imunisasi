@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Asisten Dokter</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Asisten Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <a class="btn btn-primary pull-right" href="{{route('asistendokter.create')}}" data-toggle="modal">Tambah</a>
                </div>
                <div class="box-body">
                   <table class="table table-bordered" id="tb_asdok">
                       <thead>
                           <tr>
                              <th>Id Asdok</th>
                              <th>Nama Asdok</th>
                              <th>Jenis Kelamin</th>
                              <th>Alamat</th>
                              <th>No Telepon</th>
                              <th>KTP</th>

                              <th>Aksi</th>
                           </tr>
                       </thead>
                     
                       <tbody>
                        @forelse($asdok as $as)
                          <tr>
                            <td>{{$as->id_asdok}}</td>
                            <td>{{$as->nama_asdok}}</td>
                            <td>
                              @if($as->jenis_kelamin_asdok==1)
                                Laki-Laki
                              @else
                                Perempuan
                              @endif
                            </td>
                            <td>{{$as->alamat_asdok}}</td>
                            <td>{{$as->no_tlp_asdok}}</td>
                            <td>{{$as->no_ktp_asdok}}</td>
                            <th><a href="{{route('asistendokter.edit',[$as->id_asdok])}}"><span class="btn btn-warning"><i class="fa fa-edit"></i></span></a></th>


                          </tr>
                        @empty
                        @endforelse
                       
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      var tb_asdok = $('#tb_asdok').DataTable({
        responsive:true
      });
  });
</script>
@endpush

