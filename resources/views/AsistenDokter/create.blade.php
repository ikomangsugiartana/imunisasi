@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Asisten Dokter</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Asisten Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Tambah Data Asdok</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('asistendokter.store')}}">
                  @csrf
                <div class="box-body">
                   <div class="form-group @error('nama_asdok') has-error @enderror">
                     <label class="col-md-2">Nama Asdok</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_asdok" class="form-control" value="{{old('nama_asdok')}}" placeholder="nama asdok">
                        @error('nama_asdok')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>
                    <div class="form-group @error('jenis_kelamin') has-error @enderror">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                        <select class="form-control" name="jenis_kelamin" value="{{old('jenis_kelamin')}}">
                          <option value="">Pilih Jenis Kelmain</option>
                          <option value="1" {{(old('jenis_kelamin')==1)?'selected':''}}>Laki-Laki</option>
                          <option value="0" {{(old('jenis_kelamin')==0)?'selected':''}}>Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>
                   <div class="form-group @error('alamat') has-error @enderror">
                     <label class="col-md-2">Alamat Asdok</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control">{{old('alamat')}}</textarea>
                       @error('alamat')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>
                   <div class="form-group @error('telepon') has-error @enderror">
                     <label class="col-md-2">Telepon</label>
                     <div class="col-md-10">
                       <input type="text" name="telepon" class="form-control" placeholder="telepon" value="{{old('telepon')}}">
                        @error('telepon')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>

                    <div class="form-group @error('ktp') has-error @enderror">
                     <label class="col-md-2">KTP</label>
                     <div class="col-md-10">
                       <input type="text" name="ktp" class="form-control" value="{{old('ktp')}}" placeholder="KTP">
                        @error('ktp')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>
                  <div class="form-group  @error('username') has-error @enderror">
                     <label class="col-md-2">Username</label>
                     <div class="col-md-10">
                       <input type="text" name="username" class="form-control" value="{{old('username')}}" placeholder="username">
                       @error('username')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                      
                     </div>
                      </div>

                    <div class="form-group @error('password') has-error @enderror">
                     <label class="col-md-2">Password</label>
                     <div class="col-md-10">
                       <input type="password" name="password" class="form-control" placeholder="password">
                         @error('password')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                      </div>

                        <div class="form-group">
                     <label class="col-md-2">Konfirmsi Password</label>
                     <div class="col-md-10">
                       <input type="password" name="password_confirmation" class="form-control" placeholder="konfirmasi password">
                      
                     </div>
                   </div>
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

