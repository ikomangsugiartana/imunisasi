@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Asisten Dokter</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Asisten Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Edit Data Asdok</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('asistendokter.update',[$asdok->id_asdok])}}">
                  @csrf
                  @method('PUT')
                <div class="box-body">
                   <div class="form-group">
                     <label class="col-md-2">Nama Asdok</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_asdok" class="form-control" required="" placeholder="nama asdok" value="{{$asdok->nama_asdok}}">
                     </div>
                   </div>
                    <div class="form-group">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                        <select class="form-control" required="" name="jenis_kelamin">
                          <option value="">Pilih Jenis Kelmain</option>
                          <option value="1" {{($asdok->jenis_kelamin_asdok==1)?'selected':''}}>Laki-Laki</option>
                          <option value="0" {{($asdok->jenis_kelamin_asdok==0)?'selected':''}}>>Perempuan</option>
                        </select>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-md-2">Alamat Asdok</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control">{{$asdok->alamat_asdok}}</textarea>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-md-2">Telepon</label>
                     <div class="col-md-10">
                       <input type="text" name="telepon" class="form-control" required="" placeholder="telepon" value="{{$asdok->no_tlp_asdok}}">
                       
                     </div>
                   </div>

                    <div class="form-group">
                     <label class="col-md-2">KTP</label>
                     <div class="col-md-10">
                       <input type="text" name="ktp" class="form-control" required="" placeholder="KTP" value="{{$asdok->no_ktp_asdok}}">
                      
                     </div>
                   </div>
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

