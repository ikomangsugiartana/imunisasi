@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Pasien</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Pasien</li>   
      </ol>
@stop
@section('content')

    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Edit Data Pasien</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('pasien.update',[$pasien->id_pasien])}}">
                  @csrf
                  @method('PUT')
                 <div class="box-body">
                   <div class="form-group @error('nama_pasien') has-error @enderror">
                     <label class="col-md-2">Nama Pasien</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_pasien" class="form-control" required="" value="{{$pasien->nama_pasien}}" placeholder="nama pasien">
                        @error('nama_pasien')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>

                    <div class="form-group @error('jenis_kelamin') has-error @enderror">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                        <select class="form-control" required="" name="jenis_kelamin">
                          <option value="">Pilih Jenis Kelamin</option>
                          <option value="1" {{($pasien->jenis_kelamin_pasien==1)?'selected':''}}>Laki-Laki</option>
                          <option value="0" {{($pasien->jenis_kelamin_pasien==0)?'selected':''}}>Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>

                   <div class="form-group @error('anak_ke') has-error @enderror">
                     <label class="col-md-2">Anak ke</label>
                     <div class="col-md-10">
                       <input type="number" name="anak_ke" class="form-control" required="" placeholder="anak ke" value="{{$pasien->anak_ke}}">
                        @error('anak_ke')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>

                    <div class="form-group @error('tanggal_lahir') has-error @enderror">
                     <label class="col-md-2">Tanggal Lahir</label>
                     <div class="col-md-10">
                       <input type="date" name="tanggal_lahir" class="form-control" required="" placeholder="tanggal lahir" value="{{$pasien->tanggal_lahir_pasien}}">
                       @error('tanggal_lahir')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>


                    <div class="form-group @error('nama_ayah') has-error @enderror">
                     <label class="col-md-2">Nama Ayah</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ayah" class="form-control" required="" placeholder="Nama Ayah" value="{{$pasien->nama_ayah_pasien}}">
                        @error('nama_ayah')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>


                    <div class="form-group @error('nama_ibu') has-error @enderror">
                     <label class="col-md-2">Nama Ibu</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ibu" class="form-control" required="" placeholder="Nama Ibu" value="{{$pasien->nama_ibu_pasien}}">
                        @error('nama_ibu')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>

                   <div class="form-group  @error('alamat') has-error @enderror">
                     <label class="col-md-2">Alamat</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control" placeholder="alamat" >{{$pasien->alamat_pasien}}</textarea>
                         @error('alamat')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>
                   <div class="form-group @error('no_telepon') has-error @enderror">
                     <label class="col-md-2">No Telpon </label>
                     <div class="col-md-10">
                       <input type="text" name="no_telepon" class="form-control" required="" placeholder="telepon orang tua" value="{{$pasien->nomor_orang_tua_pasien}}">
                        @error('no_telepon')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                   </div>



                    <div class="form-group form-group @error('password') has-error @enderror">
                     <label class="col-md-2">Password</label>
                     <div class="col-md-10">
                       <input type="text" name="password" class="form-control"  placeholder="password">
                      @error('password')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                        @enderror
                     </div>
                      </div>

                        <div class="form-group">
                     <label class="col-md-2">Konfirmsi Password</label>
                     <div class="col-md-10">
                       <input type="text" name="konfirmasi_password" class="form-control"  placeholder="konfirmasi password">
                      
                     </div>
                   </div>
                 </div>
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

