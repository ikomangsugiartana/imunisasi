@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>pasien</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data pasien</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Tambah Data Dokter</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('pasien.store')}}">
                  @csrf
                <div class="box-body">
                  <div class="form-group has-feedback @error('nama_pasien') has-error @enderror">
                     <label class="col-md-2">Nama Pasien</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_pasien" class="form-control" value="{{ old('nama_pasien') }}"" placeholder="nama pasien">
                         @error('nama_pasien')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                     </div>
                   </div>

                    <div class="form-group has-feedback @error('jenis_kelamin') has-error @enderror">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                        <select class="form-control" required="" name="jenis_kelamin">
                          <option value="">Pilih Jenis Kelamin</option>
                          <option value="1" {{(old('jenis_kelamin')==1)?'selected':''}}>Laki-Laki</option>
                          <option value="0" {{(old('jenis_kelamin')==0)?'selected':''}}>Perempuan</option>
                        </select>
                         @error('jenis_kelamin')
                              <span class="help-block" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                     </div>
                   </div>

                   <div class="form-group  has-feedback @error('anak_ke') has-error @enderror">
                     <label class="col-md-2">Anak ke</label>
                     <div class="col-md-10">
                       <input type="number" name="anak_ke"  value="{{ old('anak_ke') }}" class="form-control"  placeholder="anak ke">
                     @error('anak_ke')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                     </div>
                   </div>

                    <div class="form-group has-feedback @error('tanggal_lahir') has-error @enderror">
                     <label class="col-md-2">Tanggal Lahir</label>
                     <div class="col-md-10">
                       <input type="date" name="tanggal_lahir" class="form-control"  value="{{ old('tanggal_lahir') }} placeholder="tanggal lahir">
                        @error('tanggal_lahir')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                     </div>
                   </div>


                    <div class="form-group has-feedback @error('nama_ayah') has-error @enderror">
                     <label class="col-md-2">Nama Ayah</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ayah" class="form-control" value="{{ old('nama_ayah') }}" placeholder="Nama Ayah">
                      @error('nama_ayah')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror

                     </div>
                   </div>


                    <div class="form-group  has-feedback @error('nama_ibu') has-error @enderror">
                     <label class="col-md-2">Nama Ibu</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ibu" class="form-control" value="{{ old('nama_ibu') }} " placeholder="Nama Ibu">
                      @error('nama_ibu')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                     </div>
                   </div>

                   <div class="form-group has-feedback @error('nama_ayah') has-error @enderror">
                     <label class="col-md-2">Alamat</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control" placeholder="alamat">{{ old('alamat') }} </textarea>
                      @error('alamat')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                     </div>
                   </div>
                   <div class="form-group has-feedback @error('no_telepon') has-error @enderror">
                     <label class="col-md-2">No Telpon </label>
                     <div class="col-md-10">
                       <input type="text" name="no_telepon" class="form-control" value="{{ old('no_telepon') }} "  placeholder="telepon orang tua">
                        @error('no_telepon')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                     </div>
                   </div>

                    <div class="form-group has-feedback @error('username') has-error @enderror">
                     <label class="col-md-2">Username</label>
                     <div class="col-md-10">
                       <input type="text" name="username" class="form-control"  value="{{ old('username') }} " placeholder="username">
                      @error('username')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                     </div>
                      </div>

                    <div class="form-group has-feedback @error('password') has-error @enderror">
                     <label class="col-md-2">Password</label>
                     <div class="col-md-10">
                       <input type="password" name="password" class="form-control" placeholder="password"  value="{{ old('password') }}">
                                            @error('password')
                        <span class="help-block" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                      
                     </div>
                      </div>

                        <div class="form-group">
                     <label class="col-md-2">Konfirmsi Password</label>
                     <div class="col-md-10">
                       <input type="password" name="konfirmasi_password" class="form-control" placeholder="konfirmasi password">
                      
                     </div>
                   </div>
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

