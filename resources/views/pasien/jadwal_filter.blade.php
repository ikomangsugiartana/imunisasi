@extends('layouts.template')
@section('bread')
    <h1 >
        Kunjungan <small>Imunisasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Jadwal Imunisasi</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
              <div class="box-header"> 
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date" value="<?php   echo date('m/d/Y') ?>">
                </div>
              </div>
                <div class="box-body" id="loadjadwal">
                   
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script >
  $(document).ready(function(){
    $('#date').datepicker({
      autoclose: true
    });

    var date=$('#date').val();
    var link="{{url('/loadjadwal')}}?date="+date;
    $('#loadjadwal').load(link);
  });

  $('#date').change(function(){
    var date=$(this).val();
    var link="{{url('/loadjadwal')}}?date="+date;
    $('#loadjadwal').load(link);
    
  });
</script>
@endpush

