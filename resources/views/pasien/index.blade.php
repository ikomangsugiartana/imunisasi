@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Pasien</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Pasien</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <a class="btn btn-primary pull-right" href="{{route('pasien.create')}}" data-toggle="modal">Tambah</a>
                </div>
                <div class="box-body">
                   <table class="table table-bordered" id="tb_pasien">
                       <thead>
                           <tr>
                              <th>Id Pasien</th>
                              <th>Nama Pasien</th>
                              <th>Jenis Kelamin</th>
                              <th>Alamat</th>

                              <th>Aksi</th>
                           </tr>
                       </thead>
                     
                       <tbody>
                        @forelse($pasiens as $pasien)
                          <tr>
                            <td>{{$pasien->id_pasien}}</td>
                            <td>{{$pasien->nama_pasien}}</td>
                            <td>
                             @if($pasien->jenis_kelamin_pasien==1)
                                Laki-Laki
                              @else
                                Perempuan
                              @endif
                            </td>
                            <td>{{$pasien->alamat_pasien}}</td>
                            <th><a href="{{route('pasien.edit',[$pasien->id_pasien])}}"><span class="btn btn-warning"><i class="fa fa-edit"></i></span></a>

                              <a href="{{route('pasien.show',[$pasien->id_pasien])}}"><span class="btn btn-warning"><i class="fa fa-eye"></i></span></a></th>


                          </tr>
                        @empty
                        @endforelse
                       
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      var tb_asdok = $('#tb_pasien').DataTable({
        responsive:true
      });
  });
</script>
@endpush

