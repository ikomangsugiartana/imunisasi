@extends('layouts.template')
@section('bread')
    <h1>
        Detail Data <small>Pasien</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Dokter</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <form class="form-horizontal">
                
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-md-2">Nama Pasien</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_pasien" class="form-control" required="" value="{{$pasien->nama_pasien}}" placeholder="nama pasien" readonly="">
                     </div>
                   </div>
                    <div class="form-group">
                     <label class="col-md-2">Pilih Jenis kelamin</label>
                     <div class="col-md-10">
                      <input type="text" name="" class="form-control" readonly="" value="{{($pasien->jenis_kelamin_pasien==1)?'Laki-Laki':'Perempuan'}}">
                     </div>
                   </div>

                   <div class="form-group">
                     <label class="col-md-2">Anak ke</label>
                     <div class="col-md-10">
                       <input type="number" name="anak_ke" class="form-control" required="" placeholder="anak ke" value="{{$pasien->anak_ke}}" readonly="">
                     </div>
                   </div>

                    <div class="form-group">
                     <label class="col-md-2">Tanggal Lahir</label>
                     <div class="col-md-10">
                       <input type="date" name="tanggal_lahir" class="form-control" required="" placeholder="tanggal lahir" value="{{$pasien->tanggal_lahir_pasien}}" readonly="">
                     </div>
                   </div>


                    <div class="form-group">
                     <label class="col-md-2">Nama Ayah</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ayah" class="form-control" required="" placeholder="Nama Ayah" value="{{$pasien->nama_ayah_pasien}}" readonly="">
                     </div>
                   </div>


                    <div class="form-group">
                     <label class="col-md-2">Nama Ibu</label>
                     <div class="col-md-10">
                       <input type="text" name="nama_ibu" class="form-control" required="" placeholder="Nama Ibu" value="{{$pasien->nama_ibu_pasien}}" readonly="">
                     </div>
                   </div>

                   <div class="form-group">
                     <label class="col-md-2">Alamat</label>
                     <div class="col-md-10">
                       <textarea name="alamat" class="form-control" readonly="" placeholder="alamat" >{{$pasien->alamat_pasien}}</textarea>
                     </div>
                   </div>
                   <div class="form-group">
                     <label class="col-md-2">No Telpon </label>
                     <div class="col-md-10">
                       <input type="text" name="no_telepon" class="form-control" readonly="" placeholder="telepon orang tua" value="{{$pasien->nomor_orang_tua_pasien}}">
                       
                     </div>
                   </div>



                 </div>
                 
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

