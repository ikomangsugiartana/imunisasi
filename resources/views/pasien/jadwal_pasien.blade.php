@extends('layouts.template')

@section('content')
    <div class="row">
      <div class="col-md-12">
      <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pasien</span>
              <span class="info-box-number">{{$user->nama_pasien}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
               <div class="box-header with-border " style="text-align: center">
                    <h3 class="box-title">JADWAL IMUNISASI</h3>
                    <a href="#" class="btn btn-default pull-right"><i class="fa fa-print"> </i></a>
               </div>
                <div class="box-body">
                             @foreach($jadwal as $key=>$jenis)
                             <div class="col-md-2">
                              <div class="col-md-12 btn btn-default btn-flat" style="margin-bottom: 20px;">
                                {{$key}}
                               
                              </div>
                                  @foreach($jenis as $imunisasi)
                                  <?php 
                                    if ($imunisasi->id_dokter=='') {
                                      $color='bg-red';
                                    }else{
                                      $color='bg-green';

                                    }
                                   ?>
                                    <div class="col-md-12">
                                      <div class="row">
                                        @if(Auth::user()->role==2)
                                        <a href="#" data-toggle="modal" data-target="#modalJadwalImunisasi" data-id='<?= json_encode($imunisasi) ?>'>
                                        <div class="small-box <?php echo $color ?>">
                                          <div class="inner">
                                            <p>{{$imunisasi->nama_imunisasi}}</p>
                                            <p>{{$imunisasi->tgl}}</p>
                                            <p>{{$imunisasi->nama_dokter}}</p>
                                            <p>{{$imunisasi->tgl_imunisasi}}</p>


                                          </div>
                                          <div class="icon">
                                          <i class="ion ion-stats-bars"></i>
                                          </div>
                                        </div>
                                        </a>
                                        @else
                                        <div class="small-box <?php echo $color ?>">
                                          <div class="inner">
                                            <p>{{$imunisasi->nama_imunisasi}}</p>
                                            <p>{{$imunisasi->tgl}}</p>

                                          </div>
                                          <div class="icon">
                                          <i class="ion ion-stats-bars"></i>
                                          </div>
                                        </div>
                                        @endif

                                      </div>
                                    </div>
                                  @endforeach
                                </div>
                             @endforeach
                </div>
                <div class="box-footer">
                    <div class="col-md-9 well well-sm no-shadow" >
                         <p>Keterangan :</p>
                         <ol>
                              <li>Kolom pada jadwal imunisasi yang berwarna hijau artinya imunisasi diberikan dan berisikan tanggal jadwal sebenarnya, tanggal kunjungan imunisasi, nama dokter yang menangani dan umur pasien. 
                              </li>

                              <li>
                                    Kolom pada jadwal imunisasi bewarna merah artinya imunisasi belum diberikan dan berisikan tanggal dan jadwal imunisasi.
                              </li>
                              <li>
                                   Jika imunisasi terlewatkan maka kolom tidak akan berwarna.
                              </li>
                         </ol>
                    </div>
                    <div class="col-md-3">
                         <style type="text/css">
                              .pagination {
                                   margin: 0;
                              }
                              .box-header {
    color: #444;
    display: block;
    padding: 25px 10px;
    position: relative;
}
                         </style>
                         {{$jadwal->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@if(Auth::user()->role==2)
<div id="modalJadwalImunisasi" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">JADWAL IMUNISASI</h4>
      </div>
      <form method="post" action="{{route('postjadwal')}}">
        @csrf
      <div class="modal-body">
         <?php 
              $dokter=App\Dokter::where('user_id',Auth::user()->id)->first();
             ?>
          <div class="form-group">
            <label>Imunisasi</label>
            <input type="text" readonly="" name="imunisasi" value="" class="form-control" id="nama_imunisasi">
            <input type="hidden" name="id_imunisasi" value="" id="id_imunisasi">
            <input type="hidden" name="id_pasien" value="{{$user->id_pasien}}" id="id_pasien">
            <input type="hidden" name="id_dokter" value="{{$dokter->id_dokter}}">

          
          </div>

          <div class="form-group">
            <label>Dokter</label>
           
            <input type="text" name="imunisasi" value="{{$dokter->nama_dokter}}" class="form-control" readonly="">
          </div>

          <div class="form-group">
            <label>Tanggal Imunisasi</label>
            <input type="date" name="tgl_imunisasi" value="<?php echo date('m/d/Y') ?>" class="form-control">
          </div>
      </div>
      <div class="box-footer">
        <div class="col-md-12">
          <button class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
      </form>
    </div>

  </div>
</div>
@endif
@endsection

@push('scripts')
<script type="text/javascript">
       $('#modalJadwalImunisasi').on('show.bs.modal',function(e){
      $('.overlay').css('display','block');
        var id = $(e.relatedTarget).data('id');
        $('#id_imunisasi').val(id.id_imunisasi);
        $('#nama_imunisasi').val(id.nama_imunisasi);
        setTimeout(function() {
                $('.overlay').css('display','none');
        }, 1500);
         
     });
</script>
@endpush

