<table class="table table-bordered" id="tb_jadwal">
     <thead>
         <tr>
            <th>Nama Pasien</th>
            <th>Nama Imunisai</th>
            <th>Tanggal Imunisasi</th>

         </tr>
     </thead>
   
     <tbody>
     
     @forelse($jadwalpasien as $value)
 
     <tr>
       <td>    <a href="{{route('jadwalpasien',[$value->id_pasien])}}">{{$value->getPasien->nama_pasien}}     </a></td>
       <td>{{$value->getImunisasi->nama_imunisasi}}</td>
       <td>{{$value->tgl}}</td>

     </tr>

     @endforeach
     </tbody>
 </table>