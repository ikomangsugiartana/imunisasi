@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Jenis Imunisasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Jenis Imunisasi</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-8 ">
            <div class="box box-solid">
                <div class="box-body">
                   <table class="table table-bordered" id="tb_jenis">
                       <thead>
                           <tr>
                              <th>Jenis Imunisasi</th>
                              <th>Aksi</th>
                           </tr>
                       </thead>
                     
                       <tbody>
                        @forelse($jenis_imunisasi as $value)
                          <tr>
                            <td>{{$value->jenis_imunisasi}}</td>
                            <th><a href="{{route('jenisimunisasi.edit',[$value->id])}}"><span class="btn btn-warning"><i class="fa fa-edit"></i></span></a></th>


                          </tr>
                        @empty
                        @endforelse
                       
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
          <div class="box box-solid">
               <div class="box-header with-border">
                    <h3 class="box-title">Jenis Imunisasi</h3>
               </div>
               <form action="{{route('jenisimunisasi.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                         <div class="form-group">
                              <input type="text" name="jenis_imunisasi" class="form-control" required="" placeholder="jenis imunisasi">
                         </div>
                    </div>
                    <div class="box-footer">
                         <button class="btn btn-primary pull-right">Simpan</button>
                    </div>
               </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      var tb_jenis = $('#tb_jenis').DataTable({
        responsive:true
      });
  });
</script>
@endpush

