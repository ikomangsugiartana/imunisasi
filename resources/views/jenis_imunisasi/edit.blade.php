@extends('layouts.template')
@section('bread')
    <h1>
        Data <small>Jenis Imunisasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Jenis Imunisasi</li>   
      </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title center">Edit Data Jenis Imunisasi</h3>
                </div>
                <form class="form-horizontal" method="post" action="{{route('jenisimunisasi.update',[$jenis_imunisasi->id])}}">
                  @csrf
                  @method('PUT')
                <div class="box-body">
                   <div class="form-group">
                     <label class="col-md-2">Nama Imunisasi</label>
                     <div class="col-md-10">
                       <input type="text" name="jenis_imunisasi" class="form-control" required="" placeholder="nama imunisasi" value="{{$jenis_imunisasi->jenis_imunisasi}}">
                     </div>
                   </div>

                 
                   <div class="box-footer">
                     <button class="btn btn-primary pull-right">Simpan</button>
                   </div>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
@endpush

