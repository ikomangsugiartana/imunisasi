<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login2');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/supplier', 'SupplierController@index')->name('supplier.index');
Route::post('/supplier/store', 'SupplierController@store')->name('supplier.store');
Route::get('/supplier/{id}/edit', 'SupplierController@edit')->name('supplier.edit');
Route::put('/supplier/{id}/update', 'SupplierController@update')->name('supplier.update');

Route::get('/asistendokter', 'AsistenDokterController@index')->name('asistendokter.index');
Route::get('/asistendokter/create', 'AsistenDokterController@create')->name('asistendokter.create');
Route::post('/asistendokter/store', 'AsistenDokterController@store')->name('asistendokter.store');
Route::get('/asistendokter/edit/{id}', 'AsistenDokterController@edit')->name('asistendokter.edit');
Route::put('/asistendokter/update/{id}', 'AsistenDokterController@update')->name('asistendokter.update');

Route::get('/dokter', 'DokterController@index')->name('dokter.index');
Route::get('/dokter/create', 'DokterController@create')->name('dokter.create');
Route::post('/dokter/store', 'DokterController@store')->name('dokter.store');
Route::get('/dokter/edit/{id}', 'DokterController@edit')->name('dokter.edit');
Route::put('/dokter/update/{id}', 'DokterController@update')->name('dokter.update');

Route::get('/pasien', 'PasienController@index')->name('pasien.index');
Route::get('/pasien/create', 'PasienController@create')->name('pasien.create');
Route::post('/pasien/store', 'PasienController@store')->name('pasien.store');
Route::get('/pasien/edit/{id}', 'PasienController@edit')->name('pasien.edit');
Route::put('/pasien/update/{id}', 'PasienController@update')->name('pasien.update');
Route::get('/pasien/show/{id}', 'PasienController@show')->name('pasien.show');

Route::get('/imunisasi', 'ImunisasiController@index')->name('imunisasi.index');
Route::get('/imunisasi/create', 'ImunisasiController@create')->name('imunisasi.create');
Route::post('/imunisasi/store', 'ImunisasiController@store')->name('imunisasi.store');
Route::get('/imunisasi/edit/{id}', 'ImunisasiController@edit')->name('imunisasi.edit');
Route::put('/pasiimunisasien/update/{id}', 'ImunisasiController@update')->name('imunisasi.update');

Route::get('/jenisimunisasi', 'JenisImunisasiController@index')->name('jenisimunisasi.index');
Route::post('/jenisimunisasi/store', 'JenisImunisasiController@store')->name('jenisimunisasi.store');
Route::get('/jenisimunisasi/edit/{id}', 'JenisImunisasiController@edit')->name('jenisimunisasi.edit');
Route::put('/jenisimunisasi/update/{id}', 'JenisImunisasiController@update')->name('jenisimunisasi.update');

Route::get('/jadwalpasien/{id}', 'PasienController@jadwalpasien')->name('jadwalpasien');
Route::get('/jadwalfilter', 'PasienController@jadwalfilter')->name('jadwalfilter');
Route::get('/loadjadwal', 'PasienController@loadjadwal')->name('loadjadwal');

Route::post('/postjadwal', 'DokterController@postjadwal')->name('postjadwal');
Route::get('/setting', 'SettingController@index')->name('setting.index');
Route::post('/setting/store', 'SettingController@store')->name('setting.store');
Route::put('/setting/update/{id}', 'SettingController@update')->name('setting.update');
























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
