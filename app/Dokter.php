<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table='dokter';
    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='id_dokter';
}
