<?php

namespace App\Console\Commands;

use DB;
use Nexmo;
use App\Setting;
use App\JadwalImunisasi;
require 'vendor/autoload.php';
use Nasution\ZenzivaSms\Client as Sms;
use Illuminate\Console\Command;


class kirimSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kirim:SMS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
                    // \Log::info('berhasil');
        $setting=Setting::get();
        if (!$setting->isEmpty()) {
              $today= date("Y-m-d");
            $tanggalAwal=strtotime($today);
            $tanggalKirim=date('Y-m-d',strtotime('3 day',$tanggalAwal));
            $jadwal=JadwalImunisasi::with('getPasien')->where('tgl',$tanggalKirim)->get();
            foreach ($jadwal as $key => $value) {
                $sms = new Sms('36o7we', '6yukv4ffz2');
                $sms->send($value->getPasien->nomor_orang_tua_pasien, $setting[0]->pesan);
            }
        }
      

       
    }
}
