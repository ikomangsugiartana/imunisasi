<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsistenDokter extends Model
{
     protected $table='asisten_dokter';
    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='id_asdok';
}
