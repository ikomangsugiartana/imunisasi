<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table='pasien';
    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='id_pasien';
}
