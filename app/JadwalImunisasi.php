<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalImunisasi extends Model
{
    protected $table='jadwal_imunisasi';
    public $timestamps=false;

    public function getPasien()
    {
    	return $this->belongsTo('App\Pasien','id_pasien','id_pasien');
    }

    public function getImunisasi()
    {
    	return $this->belongsTo('App\Imunisasi','id_imunisasi','id_imunisasi');
    }
}
