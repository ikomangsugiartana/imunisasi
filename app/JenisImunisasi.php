<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisImunisasi extends Model
{
    protected $table='jenis_imunisasi';
    public $timestamps=false;
    public $incrementing=false;

    public function getImunisasi()
	{
	    return $this->hasMany('App\Imunisasi','jenis_imunisasi','id');
	}
}
