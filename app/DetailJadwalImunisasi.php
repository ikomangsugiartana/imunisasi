<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailJadwalImunisasi extends Model
{
    protected $table='detail_jadwal_imunisasi';
    public $timestamps=false;
    public $incrementing=false;
}
