<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imunisasi extends Model
{
    protected $table='Imunisasi';
    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='id_imunisasi';

    public function getJenisImunisasi()
    {
    	return $this->belongsTo('App\JenisImunisasi','jenis_imunisasi','id');
    }
}
