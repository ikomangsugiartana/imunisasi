<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use App\User;
use App\Pasien;
use App\Imunisasi;
use App\JenisImunisasi;
use App\JadwalImunisasi;
use Illuminate\Http\Request;
use App\Http\Requests\PasienRequest;
use App\Http\Requests\EditPasienRequest;
use Illuminate\Pagination\LengthAwarePaginator; 

class PasienController extends Controller
{
    public function index(){
    	$pasiens=Pasien::all();
    	return view('pasien.index',compact('pasiens'));
    }

   public function create(){
   	return view('pasien.create');
   }

   public function store(PasienRequest $request){
     DB::beginTransaction();
     $imunisasi=Imunisasi::all();
     try {
          $user=new User();
          $user->username=$request->username;
          $user->name=$request->nama_pasien;
          $user->role=3;
          $user->password=Hash::make($request->password);
          $user->save();

          $id_pasien=Pasien::all();
          $kode='PS';
          $kode .=str_pad(count($id_pasien)+ 1, 3, '0', STR_PAD_LEFT);
          $pasien=new Pasien();
          $pasien->id_pasien=$kode;
          $pasien->nama_pasien=$request->nama_pasien;
          $pasien->jenis_kelamin_pasien=$request->jenis_kelamin;
          $pasien->anak_ke=$request->anak_ke;
          $pasien->tanggal_lahir_pasien=$request->tanggal_lahir;
          $pasien->nama_ayah_pasien=$request->nama_ayah;
          $pasien->nama_ibu_pasien=$request->nama_ibu;
          $pasien->alamat_pasien=$request->alamat;
          $pasien->nomor_orang_tua_pasien=$request->no_telepon;
          $pasien->user_id=$user->id;
          $pasien->save();

          $jadwal_pasien=[];
          foreach ($imunisasi as $key => $value) {
            $jadwal=\Carbon\carbon::parse($request->tanggal_lahir)->addMonth($value->umur);
            $data=[
              'id_pasien'=> $kode ,
              'id_imunisasi'=>$value->id_imunisasi,
              'tgl'=>$jadwal,

            ];
            array_push($jadwal_pasien, $data);
          }
          // dd($jadwal_pasien);
          DB::table('jadwal_imunisasi')->insert($jadwal_pasien);

          DB::commit();
     } catch (\Exception $e) {
         DB::rollback();
         // dd($e);
     }

   		return redirect()->route('pasien.index');

   }

   public function edit($id){
   	$pasien=Pasien::where('id_pasien',$id)->first();
   	return view('pasien.edit',compact('pasien'));
   }


     public function update(EditPasienRequest $request,$id){
     DB::beginTransaction();
     $imunisasi=Imunisasi::all();
     try {
        $pasien=Pasien::where('id_pasien',$id)->first();
        $pasien->nama_pasien=$request->nama_pasien;
        $pasien->jenis_kelamin_pasien=$request->jenis_kelamin;
        $pasien->anak_ke=$request->anak_ke;
        $pasien->tanggal_lahir_pasien=$request->tanggal_lahir;
        $pasien->nama_ayah_pasien=$request->nama_ayah;
        $pasien->nama_ibu_pasien=$request->nama_ibu;
        $pasien->alamat_pasien=$request->alamat;
        $pasien->nomor_orang_tua_pasien=$request->no_telepon;
        if ($request->password!="") {
          $user=User::find($pasien->user_id);
          dd($user);
          $user->password=Hash::make($request->password);
          // $user->save();
        }
        // $pasien->update();

        // $delete_jadwal=DB::table('jadwal_imunisasi')->where('user_id',$pasien->user_id)->delete();
          DB::commit();
    } catch (\Exception $e) {
         DB::rollback();
         // dd($e);

         // something went wrong
     }

   		return redirect()->route('pasien.index');

     }

     public function show($id){
        	$pasien=Pasien::where('id_pasien',$id)->first();
        	return view('pasien.detail',compact('pasien'));
     }

     public function jadwalpasien($id)
     {
          $user=Pasien::where('id_pasien',$id)->first();
          $querypasein=DB::table('jadwal_imunisasi as jd')
          ->join('imunisasi as i','i.id_imunisasi','=','jd.id_imunisasi')
          ->join('jenis_imunisasi as ji','ji.id','=','i.jenis_imunisasi')
          ->leftJoin('dokter as d', 'd.id_dokter', '=', 'jd.id_dokter');
          // $id_imunisasi=$querypasein->select('jenis_imunisasi')->pluck('jenis_imunisasi');
          $data_imuninisasi=$querypasein->select('i.*','jd.*','ji.id as id_jenis','ji.jenis_imunisasi as nama_jenis','d.*')->get();



        $arr = array();
        foreach ($data_imuninisasi as $key => $item) {
           $arr[$item->nama_jenis][$key] = $item;
        }

        $perpage=6;
        $current_page = LengthAwarePaginator::resolveCurrentPage();
        $arr_data = collect($arr); // alternative. You can use helper function
        $current_jadwal = $arr_data->slice(($current_page - 1) * $perpage, $perpage)->all();
        $jadwal = new LengthAwarePaginator($current_jadwal, count($arr_data), $perpage);
        // dd($jadwal);


          return view('pasien.jadwal_pasien',compact('jadwal','user'));
     }


     public function jadwalfilter()
     {
          $jadwalpasien=JadwalImunisasi::with('getPasien','getImunisasi')->get();
          return view('pasien.jadwal_filter',compact('jadwalpasien'));
     }

     public function loadjadwal()
     {
        $date=$_GET['date'];
          $format_date= date_format(date_create($date),'Y-m-d');
        $jadwalpasien=JadwalImunisasi::with('getPasien','getImunisasi')->whereDate('tgl',$format_date)->get();
        // $jadwalpasien=JadwalImunisasi::with('getUser','getImunisasi')->get();


        return view('pasien.loadjadwal',compact('jadwalpasien'));
     }
}
