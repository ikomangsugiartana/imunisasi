<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\AsistenDokter;
use App\Http\Requests\AsistenDokterRequest;
class AsistenDokterController extends Controller
{
    public function index(){
    	$asdok=AsistenDokter::all();
    	return view('asistendokter.index',compact('asdok'));
    }

    public function create(){
    	return view('asistendokter.create');
    }

    public function store(AsistenDokterRequest $request){
    DB::beginTransaction();
     try {
          $user=new User();
          $user->username=$request->username;
          $user->name=$request->nama_dokter;
          $user->role=2;
          $user->password=Hash::make($request->password);
          $user->save();

        $id_dokter=Dokter::all();
            $kode='DK';
            $kode .=str_pad(count($id_dokter)+ 1, 3, '0', STR_PAD_LEFT);
$id_asdok=AsistenDokter::all();
            $kode='AS';
            $kode .=str_pad(count($id_asdok)+ 1, 3, '0', STR_PAD_LEFT);

        $asdok=new AsistenDokter();
        $asdok->id_asdok=$kode;

        $asdok->nama_asdok=$request->nama_asdok;
        $asdok->jenis_kelamin_asdok=$request->jenis_kelamin;
        $asdok->alamat_asdok=$request->alamat;
        $asdok->no_tlp_asdok=$request->telepon;
        $asdok->no_ktp_asdok=$request->ktp;
        $asdok->user_id=$user->id;
        $asdok->save();

          DB::commit();
     } catch (\Exception $e) {
         DB::rollback();
         // something went wrong
     }
    	 

    	return redirect()->route('asistendokter.index');

    }

    public function edit($id){
        $asdok=AsistenDokter::where('id_asdok',$id)->first();
        return view('asistendokter.edit',compact('asdok'));
    }

    public function update(Request $request,$id){
        $asdok=AsistenDokter::where('id_asdok',$id)->first();

        $asdok->nama_asdok=$request->nama_asdok;
        $asdok->jenis_kelamin_asdok=$request->jenis_kelamin;
        $asdok->alamat_asdok=$request->alamat;
        $asdok->no_tlp_asdok=$request->telepon;
        $asdok->no_ktp_asdok=$request->ktp;

        $asdok->save();
        
        return redirect()->route('asistendokter.index');

    }
}
