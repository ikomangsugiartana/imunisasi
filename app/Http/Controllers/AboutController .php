<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){
    	$imunisasi=Imunisasi::all();
    	return view('imunisasi.index',compact('imunisasi'));
    }

    public function create(){
    	return view('imunisasi.create');
    }

}
