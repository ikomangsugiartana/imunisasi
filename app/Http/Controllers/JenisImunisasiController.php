<?php

namespace App\Http\Controllers;

use App\JenisImunisasi;
use Illuminate\Http\Request;

class JenisImunisasiController extends Controller
{
    public function index()
    {
    	$jenis_imunisasi=JenisImunisasi::all();
    	return view('jenis_imunisasi.index',compact('jenis_imunisasi'));
    }

    public function create()
    {
    	return view('jenis_imunisasi.create');
    }

    public function store(Request $request)
    {
    	$jenis_imunisasi=new JenisImunisasi();
    	$jenis_imunisasi->jenis_imunisasi=$request->jenis_imunisasi;
    	$jenis_imunisasi->save();

    	return redirect()->back()->with(['message'=>'data berhasil disimpan']);
    }

    public function edit($id)
    {
    	$jenis_imunisasi=JenisImunisasi::find($id);
    	return view('jenis_imunisasi.edit',compact('jenis_imunisasi'));
    }

    public function update(Request $request,$id)
    {
    	$jenis_imunisasi=JenisImunisasi::find($id);
    	$jenis_imunisasi->jenis_imunisasi=$request->jenis_imunisasi;
    	$jenis_imunisasi->update();

    	return redirect()->route('jenisimunisasi.index')->with(['message'=>'data berhasil dirubah']);
    }
}
