<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use App\User;
use App\Dokter;
use App\JadwalImunisasi;
use Illuminate\Http\Request;
use App\Http\Requests\DokterRequest;



class DokterController extends Controller
{
     public function index(){
    	$dokter=Dokter::all();
    	return view('dokter.index',compact('dokter'));
    }

    public function create(){
    	return view('dokter.create');
    }

    public function store(DokterRequest $request){
    DB::beginTransaction();
     try {
          $user=new User();
          $user->username=$request->username;
          $user->name=$request->nama_dokter;
          $user->role=2;
          $user->password=Hash::make($request->password);
          $user->save();

        $id_dokter=Dokter::all();
            $kode='DK';
            $kode .=str_pad(count($id_dokter)+ 1, 3, '0', STR_PAD_LEFT);

        $dokter=new Dokter();
        $dokter->id_dokter=$kode;

        $dokter->nama_dokter=$request->nama_dokter;
        $dokter->jenis_kelamin_dokter=$request->jenis_kelamin;
        $dokter->alamat_dokter=$request->alamat;
        $dokter->no_tlp_dokter=$request->telepon;
        $dokter->no_ktp_dokter=$request->ktp;
        $dokter->user_id=$user->id;
        $dokter->save();

          DB::commit();
     } catch (\Exception $e) {
         DB::rollback();
         // something went wrong
     }

    	return redirect()->route('dokter.index');

    }

    public function edit($id){
        $dokter=Dokter::where('id_dokter',$id)->first();
        return view('dokter.edit',compact('dokter'));
    }

    public function update(Request $request,$id){
        $dokter=Dokter::where('id_dokter',$id)->first();

        $dokter->nama_dokter=$request->nama_dokter;
        $dokter->jenis_kelamin_dokter=$request->jenis_kelamin;
        $dokter->alamat_dokter=$request->alamat;
        $dokter->no_tlp_dokter=$request->telepon;
        $dokter->no_ktp_dokter=$request->ktp;

        $dokter->save();
        
        return redirect()->route('dokter.index');

    }


    public function postjadwal(Request $request)
    {
      
      $jadwal=JadwalImunisasi::where('id_pasien',$request->id_pasien)->where('id_imunisasi',$request->id_imunisasi)->first();
      $jadwal->id_dokter=$request->id_dokter;
      $jadwal->tgl_imunisasi=$request->tgl_imunisasi;
      $jadwal->update();
      return redirect()->back();
    }
}
