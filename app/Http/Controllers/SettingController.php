<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
    	return view('setting.index');
    }


    public function store(Request $request){
    	$setting=new Setting();
    	$setting->time=$request->time;
    	$setting->pesan=$request->pesan;
    	$setting->save();
    	return redirect()->route('setting.index');

    }


    public function update(Request $request){
    	$setting=Setting::find(1);
    	$setting->time=$request->time;
    	$setting->pesan=$request->pesan;
    	$setting->save();
    	return redirect()->route('setting.index');

    }
}
