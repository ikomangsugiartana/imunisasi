<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Pasien;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo =true;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function redirectTo()
    {
        if(Auth::user()->role==1){
            return route('dokter.index');
        }elseif(Auth::user()->role==2){
            return route('jadwalfilter');
        }elseif (Auth::user()->role==3) {
            $user=Pasien::where('user_id',Auth::user()->id)->first();
            return route('jadwalpasien',[$user->id_pasien]);
        }   
    }
}
