<?php

namespace App\Http\Controllers;
use App\Imunisasi;
use App\JenisImunisasi;
use Illuminate\Http\Request;

class ImunisasiController extends Controller
{
    public function index(){
    	$imunisasi=Imunisasi::with('getJenisImunisasi')->get();
    	return view('imunisasi.index',compact('imunisasi'));
    }

    public function create(){
        $jenis_imunisasi=JenisImunisasi::all();
    	return view('imunisasi.create',compact('jenis_imunisasi'));
    }

    public function store(Request $request){
        $id_imunisasi=Imunisasi::all();
        $kode='AS';
        $kode .=str_pad(count($id_imunisasi)+ 1, 3, '0', STR_PAD_LEFT);
    	$imunisasi=new Imunisasi();
    	$imunisasi->id_imunisasi=$kode;
    	$imunisasi->nama_imunisasi=$request->nama_imunisasi;
    	$imunisasi->jenis_imunisasi=$request->jenis_imunisasi;
    	$imunisasi->umur=$request->umur;
    	$imunisasi->save();

    	return redirect()->route('imunisasi.index');

    }

    public function edit($id){
        $jenis_imunisasi=JenisImunisasi::all();
    	$imunisasi=Imunisasi::where('id_imunisasi',$id)->first();
    	return view('imunisasi.edit',compact('imunisasi','jenis_imunisasi'));
    }

      public function update(Request $request,$id){
    
    	$imunisasi=Imunisasi::where('id_imunisasi',$id)->first();
    	$imunisasi->nama_imunisasi=$request->nama_imunisasi;
    	$imunisasi->jenis_imunisasi=$request->jenis_imunisasi;
    	$imunisasi->umur=$request->umur;
    	$imunisasi->save();

    	return redirect()->route('imunisasi.index');

    }
}
