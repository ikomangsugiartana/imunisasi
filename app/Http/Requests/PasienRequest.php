<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasienRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $data=[
            'nama_pasien'=>'required',
            'jenis_kelamin_pasien'=>'required',
            'anak_ke'=>'required',
            'tanggal_lahir'=>'required',
            'nama_ayah'=>'required',
            'nama_ibu'=>'required',
            'alamat_pasien'=>'required',
            'no_telepon'=>'required',
            'username'=>'required|unique:users,username',
            'password'=>'required|confirmed'
        ];

        return $data;

    }

      public function messages()
    {
        return [
            'nama_pasien.required' => 'nama pasien masih kosong',
            'jenis_kelamin_pasien.required' => 'jenis_kelamin belum dipilih',
            'anak_ke.required' => 'anak ke masih kosong',
             'tanggal_lahir.required'=>'tanggal lahir masih kosong',
            'nama_ayah.required'=>'nama ayah masih kosong',
            'nama_ibu.required'=>'nama ibu masih kosong',
            'alamat_pasien.required'=>'alamat pasien masih kosong',
            'no_telepon.required'=>'telepon masih kosong',
            'username.required'=>'username masih kosong',
            'username.unique'=>'username sudah digunakan',
            'password.required'=>'password masih kosong',
            'password.confirmed'=>'konfirmasi password tidak sama',

        ];
    }
}
