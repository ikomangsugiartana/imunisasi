<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DokterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $data=[
            'nama_dokter'=>'required',
            'jenis_kelamin'=>'required',
            'alamat'=>'required',
            'telepon'=>'required',
            'ktp'=>'required',
            'username'=>'required|unique:users,username',
            'password'=>'required|confirmed'
        ];

        return $data;

    }

      public function messages()
    {
        return [
            'nama_dokter.required' => 'nama pasien masih kosong',
            'jenis_kelamin.required' => 'jenis_kelamin belum dipilih',
            'alamat.required'=>'alamat pasien masih kosong',
            'telepon.required'=>'telepon masih kosong',
            'ktp.required'=>'ktp masih kosong',
            'username.required'=>'username masih kosong',
            'username.unique'=>'username sudah digunakan',
            'password.required'=>'password masih kosong',
            'password.confirmed'=>'konfirmasi password tidak sama',

        ];
    }
}
