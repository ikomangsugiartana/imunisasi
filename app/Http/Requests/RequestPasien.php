<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPasien extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pasien'=>'required',
            'jenis_kelamin_pasien'=>'required',
            'anak_ke'=>'required',
            'tanggal_lahir_pasien'='required',
            'nama_ayah_pasien'=>'required',;
            'nama_ibu_pasien'=>'required',
            'pasien->alamat_pasien'=>'required',
            'nomor_orang_tua_pasien'=>'required',
            'username'=>'required|unique:users',
            'password'=>'required'
        ];
    }

    //   public function messages()
    // {
    //     return [
    //         'email.required' => 'Email is required!',
    //         'name.required' => 'Name is required!',
    //         'password.required' => 'Password is required!'
    //     ];
    // }
}
