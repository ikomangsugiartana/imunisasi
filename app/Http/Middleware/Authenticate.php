<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login2');
        }elseif (Auth::user()->role==1) {
            return redirect()->route('dokter.index');       
            }elseif (Auth::user()->role==2) {
                return redirect()->route('jadwalfilter');
            }elseif (Auth::user()->role==3) {

          $user=App\Pasien::where('user_id',Auth::user()->id)->first();
                return redirect()->route('jadwalpasien,[user->user_id]');
            }else{
                abort(403);
            }
    }
}
