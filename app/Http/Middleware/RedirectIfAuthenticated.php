<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->role==1) {
            return redirect()->route('dokter.index');       
            }elseif (Auth::user()->role==2) {
                return redirect()->route('jadwalfilter');
            }elseif (Auth::user()->role==3) {
                return redirect()->route('jadwalpasien');
            }else{
                abort(403);
            }
        }

        return $next($request);
    }
}
